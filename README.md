# StockBuzz-Web-App
This Project's backend is built on Node.js.
Yahoo finance API used to fetch the stock's portfolio.
    Data is marshalled at the backend, to built a relevent context of stock as per a normal trader's general usage.
    Recent trends of the stock, are used to generate a prediction based on 80% Confidence score.
Front End of this web app is supported by Bootstrap(Information representation) & and visual representation is supported via nv.d3.js.